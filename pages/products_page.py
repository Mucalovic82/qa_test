import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class ProductsPage:
    def __init__(self, browser):
        self.browser = browser
    
    # LOCATORS
    PRODUCTS = (By.ID, "grid-products")
    PRICE = (By.CSS_SELECTOR, "div[class='item__bottom__prices__price']")
    PRODUCT_NAME = (By.CLASS_NAME, "item__name")
    
    # METHODS
    
    def wait_for_products(self):
        wait = WebDriverWait(self.browser, 10)
        wait.until(EC.presence_of_element_located(self.PRODUCTS))
    
    def calculate_average_price(self):
        prices = []
        prices_list = self.browser.find_elements(*self.PRICE)
        prices.extend(int(''.join(filter(str.isdigit, i.text))) for i in prices_list)
        print(sum(prices) / len(prices))

    def select_random_product(self):
        products = self.browser.find_elements(*self.PRODUCT_NAME)
        return random.choice(products)
