from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class SingleProductPage:

    def __init__(self,browser):
        self.browser = browser

    # LOCATORS
    NAME = (By.CSS_SELECTOR, "h1[itemprop='name']")

    # METHODS
    def get_product_name(self):
        WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located(self.NAME))
        return self.browser.find_element(*self.NAME)
