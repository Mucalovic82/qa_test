from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys


class MainPage:
    def __init__(self, browser):
        self.browser = browser

    # LOCATORS
    SEARCH_FIELD = (
        By.CSS_SELECTOR, "div[id='header__center__searchbox'] div input[placeholder='Unesite pojam za pretragu']")

    # METHODS
    def search_for_product(self, product_name):
        search_field = WebDriverWait(self.browser, 30).until(EC.element_to_be_clickable(self.SEARCH_FIELD))
        search_field.send_keys(product_name)
        search_field.send_keys(Keys.ENTER)
