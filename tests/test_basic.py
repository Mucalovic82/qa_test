from pages.main_page import MainPage
from pages.products_page import ProductsPage
from pages.single_product_page import SingleProductPage

import logging

logger = logging.getLogger(__name__)


def test_basic(check_status, product_type, browser):

    logger.info("Starting tests")
    main_page = MainPage(browser)
    main_page.search_for_product(product_type)

    logger.info("Searching for product")
    products_page = ProductsPage(browser)
    products_page.wait_for_products()

    logger.info("Calculate average price")
    products_page.calculate_average_price()

    logger.info("Random process selection")
    random_item = products_page.select_random_product()
    text = random_item.text

    browser.get(random_item.get_property('href'))

    logger.info("Going to random product page")
    single_product_page = SingleProductPage(browser)
    product_name = single_product_page.get_product_name()

    assert text == product_name.text
    browser.quit()


