import pytest
import sys
import requests

from selenium import webdriver
from config import config_from_json

config_data = config_from_json("conf.json", read_from_file=True)


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="debug",
                     help="supported browsers: chrome, firefox, ie, edge, debug(no headless mode)")


@pytest.fixture(scope="session")
def browser(request):
    print("\ntests are being executed on " + request.config.getoption('--browser') + " browser")
    try:
        browser_type = request.config.getoption('--browser')
        if browser_type == 'chrome':
            browser = webdriver.Chrome()
            browser.maximize_window()

        elif browser_type == 'firefox':
            browser = webdriver.Firefox()
            browser.maximize_window()

        else:
            print("not supported browser")
            sys.exit(1)

        browser.get(config_data['url'])
        yield browser
        browser.quit()

    except Exception as e:
        print(e)
        sys.exit(1)


@pytest.fixture(scope="session")
def check_status():
    r = requests.get(config_data['url'])
    assert r.status_code == 200


@pytest.fixture(scope="session")
def product_type():
    return config_data['product_type']

