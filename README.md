# Florence QA test
Author: Nemanja Mucalovic

# Prerequisites
Setup was tested on Ubuntu 20.04 with python3.8.
It is assumed that repo was clonned on local machine.From python side it is necessary to have installed virtualenv lib.

# Instructions 

1. Download necessary drivers (chrome and firefox) and place them in directory that is on PATH
```shell
wget https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/117.0.5938.149/linux64/chromedriver-linux64.zi
unzip chromedriver-linux64.zip -d ~/.local/bin/

wget https://github.com/mozilla/geckodriver/releases/download/v0.33.0/geckodriver-v0.33.0-linux64.tar.gz
tar -xzvf geckodriver-v0.33.0-linux64.tar.gz -C ~/.local/bin
```
2. Create python virtualenv inside repo root directory and activate it
```shell
python3 -m virtualenv venv
source venv/bin/activate
```
3. Install necessary python dependencies:
```shell
pip install -r requirements.txt
```
4. This step is optional, in conf.json you can change product_type


5. Run tests
```shell
pytest -s --browser chrome
```

Notes: it is possible also to use firefox browser by changing browser cli parameter.
